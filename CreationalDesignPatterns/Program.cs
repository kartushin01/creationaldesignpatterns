﻿using CreationalDesignPatterns.Entities;
using System;

namespace CreationalDesignPatterns
{
    class Program
    {
        static void Main(string[] args)
        {


            var kayak1 = new Kayak(1);

            var kayak2 = kayak1.Copy<Kayak>();
            kayak2.PersonCount = 2;
            kayak2.HasEngine = true;

            var baggy1 = new Buggy(true, "Super 4x4", 101);

            var baggy2 = baggy1.Copy<Buggy>();
            baggy2.Model = "Puper 4x2";
            baggy2.Engine = new Engine(99);
            baggy2.IsOffRoad = false;

            var car1 = new Car("Tesla", 300);

            var car2 = car1.Clone() as Car;
            car2.Model = "BMW";
            car2.Engine = new Engine(250);

            Console.WriteLine(kayak1);
            Console.WriteLine(kayak2);

            Console.WriteLine(baggy1);
            Console.WriteLine(baggy2);

            Console.WriteLine(car1);
            Console.WriteLine(car2);


            Console.ReadKey();

        }
    }
}
