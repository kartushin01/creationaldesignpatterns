using System;

namespace CreationalDesignPatterns.Interfaces
{
    public interface IMyCloneable : ICloneable
    {
        T Copy<T>() where T : class, IMyCloneable;
    }
}