﻿using System;
using System.Collections.Generic;
using System.Text;
using CreationalDesignPatterns.Interfaces;

namespace CreationalDesignPatterns.Entities
{
    public class Engine 
    {
        public int Power { get; set; }

        public Engine(int power)
        {
            Power = power;
        }

       
    }
}
