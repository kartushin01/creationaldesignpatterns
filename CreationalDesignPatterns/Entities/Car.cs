﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CreationalDesignPatterns.Entities
{
    public class Car : Vehicle
    {
        public string Model { get; set; }
        public Engine Engine { get; set; }
        public Car(string model, int power) : base(true)
        {
            Model = model;
            Engine = new Engine(power);
        }

        
        public override object Clone()
        {
            return Copy<Car>();
        }

        public override T Copy<T>()
        {
            return new Car(Model, Engine.Power) as T;
        }

        public override string ToString()
        {
            return $"Класс Car. Наличие колес: {HasWheels}. Модель: {Model}, Мощность двигателя: {Engine.Power}";
        }
    }
}
