namespace CreationalDesignPatterns.Entities
{
    public class Kayak : Boat
    {
        public int PersonCount { get; set; }

        public Kayak(int personCount) : base(false)
        {
            PersonCount = personCount;
        }
        
        public override object Clone()
        {
            return Copy<Kayak>();
        }

        public override T Copy<T>()
        {
            return new Kayak(PersonCount) as T;
        }

        public override string ToString()
        {
            return $"Класс Kayak. Имеет ли мотор: {HasEngine}, Количество мест  : {PersonCount} ";
        }
    }
}