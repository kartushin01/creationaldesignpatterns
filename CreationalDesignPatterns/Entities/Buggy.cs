namespace CreationalDesignPatterns.Entities
{
    public class Buggy : Car
    {
        public bool IsOffRoad { get; set; }

       
        public Buggy(bool isOffRoad, string model, int power) : base(model, power)
        {
            IsOffRoad = isOffRoad;
        }
        
        public override object Clone()
        {
            return Copy<Buggy>();
        }

        public override T Copy<T>()
        {
            return new Buggy(IsOffRoad, Model, Engine.Power) as T;
        }
        
        public override string ToString()
        {
            return $"Класс Buggy. Наличие колес: {HasWheels}. Модель: {Model}. Это внедорожник? {IsOffRoad}";
        }
    }
}