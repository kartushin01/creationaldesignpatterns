﻿using System;
using System.Collections.Generic;
using System.Text;
using CreationalDesignPatterns.Interfaces;

namespace CreationalDesignPatterns.Entities
{
    public class Vehicle : IMyCloneable
    {
        public bool HasWheels { get; set; }

        public Vehicle()
        {
            
        }
        public Vehicle(bool hasWheels)
        {
            HasWheels = hasWheels;
        }
        
        public virtual object Clone()
        {
            return (object) Copy<Vehicle>();
        }

        public virtual T Copy<T>() where T : class, IMyCloneable
        {
            return new Vehicle(HasWheels) as T;
        }
    }
}
