﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CreationalDesignPatterns.Entities
{
    public class Boat : Vehicle
    {
        public bool HasEngine { get; set; }

        public Boat(bool hasEngine) : base(false)
        {
            HasEngine = hasEngine;
        }
        
        public override object Clone()
        {
            return Copy<Boat>();
        }

        public override T Copy<T>()
        {
            return new Boat(HasEngine) as T;
        }
        public override string ToString()
        {
            return $"Класс Boat. Наличие колес: {HasWheels}. Имеет мотор? {HasEngine}";
        }
    }
}
